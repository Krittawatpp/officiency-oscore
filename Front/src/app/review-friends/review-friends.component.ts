import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-review-friends',
  templateUrl: './review-friends.component.html',
  styleUrls: ['./review-friends.component.css']
})
export class ReviewFriendsComponent implements OnInit {
  @Input()
  user: any;
  skills: [];
  workshopId: any;

  // public show: boolean = false;
  // public buttonName: any = 'Review';

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    private backendService: BackendService
  ) { }

  ngOnInit() {
    this.httpclient
      .get(
        'http://localhost:8555/workshoptoreview/' +
        this.backendService.getWorkshopAttId()
      )
      .subscribe(res => {
        this.skills = res as any;
      });
  }

  submit() {
    const addUserAttWorkshop: FormData = new FormData();
    this.workshopId = this.backendService.getWorkshopAttId().toString();
    addUserAttWorkshop.append('user_id', this.user.userId);
    addUserAttWorkshop.append('workshop_att_id', this.workshopId);
  }

  // toggle() {
  //   this.show = !this.show;

  //   CHANGE THE NAME OF THE BUTTON.
  //   if (this.show)
  //     this.buttonName = "Hide";
  //   else
  //     this.buttonName = "Review";
  // }
}
