import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';
import { Workshop } from '../models/workshop';
import { User } from '../models/user';

@Component({
    selector: 'app-qrcode',
    templateUrl: './qrcode.component.html',
    styleUrls: ['./qrcode.component.css']
})
export class QRCodeComponent implements OnInit {
    vCardData: String; // Custom embeded Data in QRCode
    workshop: Workshop; // This workshop with id, title, desc,.etc.
    creator: User; // User who created this workshop.
    url: String = 'http://localhost:8555/join/';

    constructor(private backendService: BackendService) { 
        
    }

    ngOnInit() {
        if(!this.setWorkshop() || !this.setCreator()) {
            console.error('QRCode will be generated with no data');
            this.genQRCode();//QRCode will be created with no data
        }else {
            this.genQRCode();
        }
    }

    setWorkshop() :Boolean{
        this.workshop = this.backendService.getWorkshop();
        //TODO: Workshop can't be empty, undefied or null.
        if(!this.workshop) {
            console.error("Workshop id is required for QRCODE! and must not be undefied");
            return false;
        }else {
            return true;
        }
    }

    setCreator() :Boolean{
        this.creator = this.backendService.getUser();
         //TODO: Creator can't be empty, undefied or null.
        if(!this.creator) {
            console.error("Creator is required for QRCODE and must not be undefied!");
            return false;
        }else {
            return true;
        }
    }

//Both Workshop and Creator are requied, 
//default value will be assigned them, if they are undefied.
    genQRCode() {
        let workshopID:number, workshopTitle:String, workshopDesc:String, 
       creatorID:number, creatorFistName:String, creatorLastName:String, 
        creatorEmail:String, creatorTel:String;

        console.log("Starting to generate QRCode...");
        if(this.workshop && this.creator) {
            workshopID = this.workshop.workshopId;
            workshopTitle = this.workshop.title;
            workshopDesc = this.workshop.description;
            creatorID = this.creator.id;
             creatorFistName = this.creator.firstname;
             creatorLastName = this.creator.lastname;
             creatorEmail = this.creator.email;
             creatorTel = this.creator.tel;
        } else{
            //Use fake data instead
            workshopID = 3;
            workshopTitle = 'Spring Boot Framwork';
            workshopDesc = 'N/A';
            creatorID = 3;
             creatorFistName = 'Max';
             creatorLastName = 'Liverpool';
             creatorEmail = 'max@mm.co';
             creatorTel = 'N/A';
        }


        let data = {
            begin: `BEGIN:VCARD\n`,
            version: `VERSION:3.0\n`,
            name: `N:${workshopDesc};${workshopTitle}\n`,
            fullname: `FN:${creatorFistName} ${creatorLastName}\n`,
            org: `ORG:Officiency OSCore\n`,
            url: `URL:${this.url}${workshopID}\n`,
            email: `EMAIL:${creatorEmail}\n`,
            tel: `TEL;TYPE=voice,work,oref:${creatorTel}\n`,
            end: `END:VCARD\n`
        }

        this.vCardData = data.begin + data.version + data.name + data.fullname + data.org + data.url + data.email + data.tel + data.end;
        console.log(this.vCardData);
    }

} 