import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';
import { User } from '../models/user';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  userId: any;
  editform: FormGroup;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private backendService: BackendService,
    private router: Router
  ) {
    this.editform = this.fb.group({
      email: ['', Validators.email],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      nickname: ['', Validators.required],
      tel: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.userId = this.backendService.getUser().id;
  }

  submitForm() {
    const edit: FormData = new FormData();
    edit.append('user_id', this.userId);
    edit.append('email', this.editform.get('email').value);
    edit.append('firstname', this.editform.get('firstname').value);
    edit.append('lastname', this.editform.get('lastname').value);
    edit.append('nickname', this.editform.get('nickname').value);
    edit.append('tel', this.editform.get('tel').value);
    this.httpClient
      .post('http://localhost:8555/editprofile', edit)
      .subscribe(res => {
        alert('edit profile successful!');
      });
  }
}
