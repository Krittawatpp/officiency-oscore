import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-add-workshop',
  templateUrl: './add-workshop.component.html',
  styleUrls: ['./add-workshop.component.css']
})
export class AddWorkshopComponent implements OnInit {
  form: FormGroup;
  workshop: any;

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    private backendService: BackendService
  ) {
    this.form = this.fb.group({
      workshop: '',
      date: ''
    });
  }

  ngOnInit() {}

  submitForm() {
    const addWorkshop: FormData = new FormData();

    addWorkshop.append(
      'create_by_id',
      this.backendService.getUser().id.toString()
    );
    addWorkshop.append('title', this.form.get('workshop').value);
    addWorkshop.append('daystart', this.form.get('date').value);

    this.httpclient
      .post('http://localhost:8555/api/addNewWorkshop/', addWorkshop)
      .subscribe(res => {
        this.workshop = res as any[];
        console.log(this.workshop);
        let workshop_id = this.workshop['workshopId'];
        let create_by_id = this.workshop['createById'];
        let title = this.workshop['title'];
        let description = this.workshop['description'];
        let start = this.workshop['start'];
        let end = this.workshop['end'];
        let enabled = this.workshop['enabled'];
        this.backendService.setupWorkshop(
          workshop_id,
          create_by_id,
          title,
          description,
          start,
          end,
          enabled
        );
        console.log(
          'test! save workshop to backendservice : ' +
            this.backendService.getWorkshop()
        );
        console.log(
          'TEST save workshop id to backendservice : ' +
            this.backendService.getWorkshop().workshopId
        );
      });
  }
}
