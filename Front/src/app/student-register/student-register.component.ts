import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-student-register',
  templateUrl: './student-register.component.html',
  styleUrls: ['./student-register.component.css']
})
export class StudentRegisterComponent implements OnInit {
  formRegister: FormGroup;
  users: any;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private backendService: BackendService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {}

  createForm() {
    this.formRegister = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]), //TODO: Match pattern with Spring FrameWork
      cfpassword: new FormControl('', [Validators.required]) //TODO: Match pattern with Spring FrameWork
    });
  }

  submitForm() {
    console.log('FormRegister:', this.formRegister);
    if (this.formRegister.valid) {
      alert('Valid');
      const jsonUser = JSON.stringify(this.formRegister.value);

      const data: FormData = new FormData();
      data.append('email', this.formRegister.get('email').value);
      data.append('password', this.formRegister.get('password').value);
      data.append('confirmpassword', this.formRegister.get('cfpassword').value);

      this.httpClient
        .post('http://localhost:8555/registerStudent', data)
        .subscribe(result => {
          this.users = result as any[];
          console.log(this.users);
          let userId = this.users['userId'];
          let email = this.users['email'];
          let firstname = this.users['firstName'];
          let lastname = this.users['lastName'];
          let nickname = this.users['nickName'];
          let tel = this.users['tel'];

          this.backendService.setupStudent(
            userId,
            email,
            firstname,
            lastname,
            nickname,
            tel
          );
          this.router.navigateByUrl('/addusergroup');
        });
    } else {
      alert('Invalid');
    }
  }
}
