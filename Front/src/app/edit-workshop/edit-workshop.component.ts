import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-workshop',
  templateUrl: './edit-workshop.component.html',
  styleUrls: ['./edit-workshop.component.css']
})
export class EditWorkshopComponent implements OnInit {
  form: FormGroup;
  workshop: any;

  constructor(private httpclient: HttpClient, private backendService: BackendService,
    private fb: FormBuilder, private router: Router) {
    this.form = this.fb.group({
      title: ['', Validators.required],
      desc: ['', Validators.required],
      start: ['', Validators.required],
      end: ['', Validators.required]
    });

    this.workshop = backendService.getWorkshop();

    this.form.setValue({
      title: this.workshop.title,
      desc: this.workshop.description,
      start: this.workshop.start,
      end: this.workshop.end
    });

  }

  ngOnInit() {
    console.log(this.workshop.workshopId);
  }

  editForm() {
    const payload: FormData = new FormData();

    payload.append('workshopId', this.workshop.workshopId);
    payload.append('title', this.form.get('title').value);
    payload.append('description', this.form.get('desc').value);
    payload.append('start', this.form.get('start').value);
    payload.append('end', this.form.get('end').value);

    this.httpclient
      .post('http://localhost:8555/workshop/editWorkshop/', payload)
      .subscribe(result => {
        this.workshop = result as any;
        alert("Edit Done!!!");

        // เราเตอร์ลิ้งไปหน้า addgroup 
        this.router.navigate(['manage'])
      });
  }

  deleteForm() {
    const payload: FormData = new FormData();

    payload.append('workshopId', this.workshop.workshopId);

    this.httpclient
      .post('http://localhost:8555/workshop/deleteWorkshop/', payload)
      .subscribe(result => {
        this.workshop = result as any;

        // เราเตอร์ลิ้งไปหน้า addgroup 
        this.router.navigate(['manage'])
      });
  }

}
