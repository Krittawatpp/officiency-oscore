import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-self-summary',
  templateUrl: './self-summary.component.html',
  styleUrls: ['./self-summary.component.css']
})
export class SelfSummaryComponent implements OnInit {
  studentScores: any;
  studentFeedbacks: any;
  studentProfile: any;

  constructor(
    private httpClient: HttpClient,
    private backendService: BackendService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.getStudentProfile();
    this.getStudentSkillfromCurrentWorkshop();
    this.getStudentFeedbackfromCurrentWorkshop();
  }

  getStudentProfile() {
    this.httpClient
      // hardcoe
      // .get('http://localhost:8555/api/getUserById/17')
      .get(
        'http://localhost:8555/api/getUserById/' +
          this.backendService.getStudent().id
      )
      .subscribe(res => {
        this.studentProfile = res as any;
      });
  }

  getStudentSkillfromCurrentWorkshop() {
    this.httpClient
      // hardcode
      // .get(
      //   'http://localhost:8555/api/userscorebyuseridandworkshopid/17/1'
      // di code
      .get(
        'http://localhost:8555/api/userscorebyuseridandworkshopid/' +
          this.backendService.getStudent().id +
          '/' +
          this.backendService.getWorkshopAttId()
      )
      .subscribe(res => {
        this.studentScores = res as any;
      });
  }

  getStudentFeedbackfromCurrentWorkshop() {
    this.httpClient
      // hardcode
      // .get(
      //   'http://localhost:8555/student/showUserFeedbackForCurrentWorkshop/17/1'
      // )
      // di code
      .get(
        'http://localhost:8555/student/showUserFeedbackForCurrentWorkshop/' +
          this.backendService.getStudent().id +
          '/' +
          this.backendService.getWorkshopAttId()
      )
      .subscribe(res => {
        this.studentFeedbacks = res as any[];
      });
  }
}
