import { Component, OnInit, Input } from '@angular/core';
import { BackendService } from '../backend.service';
import { Workshop } from '../models/workshop';

@Component({
  selector: 'app-qrcode-goqr',
  templateUrl: './qrcode-goqr.component.html',
  styleUrls: ['./qrcode-goqr.component.css']
})
export class QRCodeGoQr implements OnInit {
  @Input()
  workshop: Workshop;
  webUrl: String = 'http://localhost:4200/studentlogin/';
  apiUrl: String;

  constructor(private backendService: BackendService) {
    this.backendService.setAllWorkshops();
  }

  ngOnInit() {
    this.webUrl = encodeURI(this.webUrl + String(this.workshop.workshopId));
    this.apiUrl = `https://api.qrserver.com/v1/create-qr-code/?data=${
      this.webUrl
    }&amp;size=100x100`;
    //let divQRcode = document.getElementById("qrcode-goqr");
    // divQRcode.innerHTML += `<a href="${this.apiUrl}" target="_blank"><img src="assets/qr-code.svg"></a>`;
  }

  test() {
    alert('QRCode is clicked.');
    this.backendService.setWorkshopForReview(this.workshop);
  }
}
