import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-show-user-in-group',
  templateUrl: './show-user-in-group.component.html',
  styleUrls: ['./show-user-in-group.component.css']
})
export class ShowUserInGroupComponent implements OnInit {
  users: [];

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    private backendService: BackendService
  ) { }

  ngOnInit() {
    this.httpclient
      .get(
        'http://localhost:8555/student/showStudentInGroup/' +
        this.backendService.getWorkshopAttId() +
        '/' +
        this.backendService.getGroup()
      )

      // .get(
      //   'http://localhost:8555/student/showStudentInGroup/' +
      //   '60' +
      //   '/' +
      //   '2'
      // )
      .subscribe(res => {
        this.users = res as any;
      });
  }
}
