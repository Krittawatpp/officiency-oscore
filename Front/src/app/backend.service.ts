import { Injectable } from '@angular/core';
import { User } from './models/user';
import { Workshop } from './models/workshop';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  public user: User;
  public workshop: Workshop;
  public allWorkshops: Workshop[];
  public group: any;
  public workshopForReview: Workshop;
  public workshopAttId: any;
  public student: User;

  constructor(private httpclient: HttpClient) {
    console.log("BackendService starting...")
  }

  setupUser(
    userId: number,
    email: string,
    firstname: String,
    lastname: String,
    nickname: String,
    tel: String
  ) {
    this.user = new User(userId, email, firstname, lastname, nickname, tel);
  }

  getUser() {
    return this.user;
  }

  setupWorkshop(
    workshopId: number,
    createById: number,
    title: string,
    description: string,
    start: string,
    end: string,
    enabled: number
  ) {
    this.workshop = new Workshop(
      workshopId,
      createById,
      title,
      description,
      start,
      end,
      enabled
    );
  }

  getWorkshop() {
    return this.workshop;
  }

  setAllWorkshops(): void {
    this.httpclient
      .get<Workshop[]>(
        'http://localhost:8555/api/getWorkShops/' + this.user.id.toString()
      )
      .subscribe(result => {
        this.allWorkshops = result;
        console.log(JSON.stringify(result));
      });
  }

  setupStudent(
    userId: number,
    email: string,
    firstname: String,
    lastname: String,
    nickname: String,
    tel: String
  ) {
    this.student = new User(userId, email, firstname, lastname, nickname, tel);
  }

  getStudent() {
    return this.student;
  }

  setupGroup(group: any) {
    this.group = group;
  }

  getGroup() {
    return this.group;
  }

  getAllWorkshops(): Workshop[] {
    return this.allWorkshops;
  }

  setWorkshopForReview(workshopForReview: Workshop): void {
    this.workshopForReview = workshopForReview;
  }

  getWorkshopForReview(): Workshop {
    return this.workshopForReview;
  }

  setWorkshopAttId(workshopId: any) {
    this.workshopAttId = workshopId;
  }

  getWorkshopAttId() {
    return this.workshopAttId;
  }
}
