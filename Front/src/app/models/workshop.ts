export class Workshop {
  public workshopId: number;
  public createById: number;
  public title: String;
  public description: String;
  public start: String;
  public end: String;
  public enabled: number;

  constructor(
    workshopId: number,
    createById: number,
    title: String,
    description: String,
    start: String,
    end: String,
    enabled: number
  ) {
    this.workshopId = workshopId;
    this.createById = createById;
    this.title = title;
    this.description = description;
    this.start = start;
    this.end = end;
    this.enabled = enabled;
  }
}
