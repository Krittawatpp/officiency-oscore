import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { BackendService } from '../backend.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leave-or-review-group',
  templateUrl: './leave-or-review-group.component.html',
  styleUrls: ['./leave-or-review-group.component.css']
})
export class LeaveOrReviewGroupComponent implements OnInit {
  workshopGroups: [];
  studentId: any;
  workshopIdForApi: any;
  groupNumber: any;
  users: any;

  constructor(
    private httpclient: HttpClient,
    private fb: FormBuilder,
    private backendService: BackendService,
    private router: Router
  ) {}

  ngOnInit() {
    // this.studentId = 17;
    // this.workshopIdForApi = 2;
    this.studentId = this.backendService.getStudent().id.toString();
    this.workshopIdForApi = this.backendService.getWorkshopAttId();
    this.showUser();
  }

  // showUser() {
  //   this.httpclient
  //     .get('http://localhost:8555/student/showStudentInGroup/2/2')
  //     .subscribe(res => {
  //       this.users = res as any;
  //     });
  // }

  showUser() {
    this.httpclient
      .get(
        'http://localhost:8555/student/showStudentInGroup/' +
          this.backendService.getWorkshopAttId() +
          '/' +
          this.backendService.getGroup()
      )
      .subscribe(res => {
        this.users = res as any;
      });
  }

  leaveGroup() {
    const data: FormData = new FormData();

    data.append('user_id', this.studentId);
    data.append('workshop_id', this.workshopIdForApi);

    this.httpclient
      .post('http://localhost:8555/api/student/leavegroup/', data)
      .subscribe(res => {
        alert('you already leave group!');
      });
    // ไปหน้า add group อีกรอบ แต่เป็น component ใหม่
    this.router.navigate(['addusergroupafterleavegroup']);
  }
}
