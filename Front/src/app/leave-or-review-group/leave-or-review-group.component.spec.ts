import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveOrReviewGroupComponent } from './leave-or-review-group.component';

describe('LeaveOrReviewGroupComponent', () => {
  let component: LeaveOrReviewGroupComponent;
  let fixture: ComponentFixture<LeaveOrReviewGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveOrReviewGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveOrReviewGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
