import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserGroupAfterLeaveGroupComponent } from './add-user-group-after-leave-group.component';

describe('AddUserGroupAfterLeaveGroupComponent', () => {
  let component: AddUserGroupAfterLeaveGroupComponent;
  let fixture: ComponentFixture<AddUserGroupAfterLeaveGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddUserGroupAfterLeaveGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserGroupAfterLeaveGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
