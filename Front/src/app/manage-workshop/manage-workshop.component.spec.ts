import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageWorkshopComponent } from './manage-workshop.component';

describe('ManageWorkshopComponent', () => {
  let component: ManageWorkshopComponent;
  let fixture: ComponentFixture<ManageWorkshopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageWorkshopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageWorkshopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
