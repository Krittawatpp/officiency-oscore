import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-manage-workshop',
  templateUrl: './manage-workshop.component.html',
  styleUrls: ['./manage-workshop.component.css']
})
export class ManageWorkshopComponent implements OnInit {
  workshops = [];

  constructor(private http: HttpClient, private backendService: BackendService) { }

  ngOnInit() {
    this.loadWorkshop();
  }

  loadWorkshop() {
    this.http
      .get('http://localhost:8555/api/getWorkShops/' + this.backendService.getUser().id)
      .subscribe(res => {
        this.workshops = res as any[];
      });
  }

  editAsWorkshop(workshop) {
    this.backendService.setupWorkshop(
      workshop.workshopId,
      workshop.createById,
      workshop.title,
      workshop.description,
      workshop.start,
      workshop.end,
      workshop.enabled
    );
    console.log(workshop);
  }

}
