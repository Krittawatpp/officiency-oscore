-- average skill score
SELECT MIN(user_att_workshop.id) as id, user_att_workshop.user_id, skill.skill_name, AVG(user_att_workshop.skill_score)
from user_att_workshop left join skill on user_att_workshop.skill_id = skill.id
GROUP BY user_att_workshop.user_id,skill.id
HAVING user_att_workshop.user_id = ?1
ORDER BY user_att_workshop.user_id;user_att_workshop