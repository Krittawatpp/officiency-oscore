-- -- USER ROLE 
-- CREATE TABLE user_role
-- (
--     id INT
--     auto_increment,
--     role_name VARCHAR
--     (50) NOT NULL,
--     PRIMARY KEY
--     (id)
-- );

-- USER

-- CREATE TABLE user
-- (
--     id INT
--     auto_increment,
-- first_name VARCHAR
--     (40) NOT NULL,
-- last_name VARCHAR
--     (40) NOT NULL,
-- email VARCHAR
--     (50) NOT NULL,
-- password VARCHAR
--     (70) NOT NULL,
-- user_role VARCHAR NOT NULL,
-- PRIMARY KEY
--     (id),
-- UNIQUE
--     (first_name, last_name)
-- );

create table user
(
    id int
    auto_increment PRIMARY KEY,
    email varchar
    (32) UNIQUE NOT NULL,
    firstname VARCHAR
    (32),
    lastname VARCHAR
    (32),
    nickname VARCHAR
    (32),
    tel VARCHAR
    (32),
    password VARCHAR
    (70) NOT NULL,
    user_role VARCHAR
    (32)
);

    -- SKILL
    CREATE TABLE skill
    (
        id INT
        auto_increment,
    skill_name VARCHAR
        (40) NOT NULL,
    PRIMARY KEY
        (id)
);

        -- WORKSHOP
        CREATE TABLE workshop
        (
            id INT
            auto_increment,
            create_by_id INT,
    title VARCHAR
            (20) NOT NULL,
    description VARCHAR
            (1200) NOT NULL,
    start DATETIME NOT null,
            end DATETIME NOT NULL,
    PRIMARY KEY
            (id),
    UNIQUE
            (title),
            FOREIGN KEY
            (create_by_id) REFERENCES user
            (id)
);

            -- WORKSHOP SKILL (relation)
            CREATE TABLE workshop_skill
            (
                workshop_id INT,
                skill_id INT,
                FOREIGN KEY (workshop_id) REFERENCES workshop (id),
                FOREIGN KEY (skill_id) REFERENCES skill (id)
            );

            -- WORKSHOP GROUP
            CREATE TABLE workshop_group
            (
                id INT
                auto_increment,
    workshop_id INT,
    group_name VARCHAR
                (50),
    PRIMARY KEY
                (id),
    FOREIGN KEY
                (workshop_id) REFERENCES workshop
                (id)
);

                -- USER ATT WORKSHOP
                CREATE TABLE user_att_workshop
                (
                    user_id INT,
                    workshop_att_id INT,
                    group_id INT,
                    skill_id INT,
                    skill_score INT,
                    FOREIGN KEY (user_id) REFERENCES user (id),
                    FOREIGN KEY (workshop_att_id) REFERENCES workshop (id),
                    FOREIGN KEY (group_id) REFERENCES workshop_group (id),
                    FOREIGN KEY (skill_id) REFERENCES skill (id)
                );
                -- user feedback
                CREATE table user_feedback
                (
                    user_id INT,
                    workshop_attend_id INT,
                    feedback VARCHAR (1200),
                    suggestion VARCHAR (1200),
                    FOREIGN KEY (user_id)
REFERENCES user (id),
                    FOREIGN KEY (workshop_attend_id)
REFERENCES workshop (id)
                );
-- -- USER SKILL 
-- CREATE TABLE user_skill
-- (
--     user_id INT,
--     skill_id INT,
--     skill_score INT,
--     FOREIGN KEY (user_id) REFERENCES user (id),
--     FOREIGN KEY (skill_id) REFERENCES skill (id)
-- );

