// package boot.controller;

// import java.security.Principal;
// import org.springframework.security.core.Authentication;
// import org.springframework.security.core.userdetails.User;
// import org.springframework.web.servlet.mvc.support.RedirectAttributes;
// import org.springframework.web.servlet.view.RedirectView;

// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.ResponseEntity;
// import org.springframework.ui.Model;
// import org.springframework.web.bind.annotation.CrossOrigin;
// import org.springframework.web.bind.annotation.PostMapping;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RequestParam;
// import org.springframework.web.bind.annotation.RestController;

// import boot.entity.user.AppUser;
// import boot.service.user.AppUserService;
// import boot.util.PasswordUtil;

// @RestController
// @CrossOrigin
// public class loginRegistorController {

//     @Autowired
//     private AppUserService appUserService;

//     // Instructor Register by 'AngryCode'
//     // ตรวจสอบข้อมูล input จากผู้ใช้ก่อนเก็บลงฐานข้อมูล และส่งกลับข้อมูลผู้ใช้ให้กับ
//     // frontend
//     @PostMapping(path = "/registerInstructor")
//     public ResponseEntity<AppUser> registerInstructor(@RequestParam("email") String email,
//             @RequestParam("firstname") String firstName, @RequestParam("lastname") String lastName,
//             @RequestParam("tel") String tel, @RequestParam("password") String password,
//             @RequestParam("confirmpassword") String confirmPassword) {

        
//         boolean validateResult = appUserService.validateInstructor(email, firstName, lastName, tel, password, confirmPassword);

//         System.out.println(" ********** Instructor Validation Result : " + validateResult);

//         AppUser user = new AppUser();

//         if (validateResult == true) {
            
//             String passEncrypted = new PasswordUtil().encryptPassword(password);
//             String role = "instructor";

//             user = appUserService.createNewInstructor(email, firstName, lastName, tel, passEncrypted, role);
//             return new ResponseEntity<>(user, HttpStatus.OK);

//         } else {
//             return new ResponseEntity<>(HttpStatus.ACCEPTED);
//         }

//     }

//     // Student Register by 'AngryCode'
//     // ตรวจสอบข้อมูล input จากผู้ใช้ก่อนเก็บลงฐานข้อมูล และส่งกลับข้อมูลผู้ใช้ให้กับ
//     // frontend
//     @PostMapping(path = "/registerStudent")
//     public ResponseEntity<AppUser> registerStudent(@RequestParam("email") String email, @RequestParam("password") String password) {
//         if(email.equals("") || password.equals("")) return new ResponseEntity<>(HttpStatus.ACCEPTED);
//         ResponseEntity<AppUser> responseUser;
//         AppUser student = appUserService.findUserByEmail(email);
//         boolean validateResult = appUserService.validateStudent(email, password);
//         System.out.println(" ********** Student Register Validation : " + validateResult);
        
//         if (validateResult == true && student == null) {
//             String passEncrypted = new PasswordUtil().encryptPassword(password);
//             String role = "student";
            
//             student = appUserService.createNewStudent(email, passEncrypted, role);
//             if(student != null) System.out.println("Register Success");
//             return responseUser = new ResponseEntity<>(student, HttpStatus.OK);

//         } else if(student.getEmail().indexOf("@") != -1){
//             System.err.println("Registration aborted. Email "+student.getEmail()+" is already registered!");
//            return responseUser =  new ResponseEntity<>(HttpStatus.ACCEPTED);
//         }

//         return responseUser =  new ResponseEntity<>(HttpStatus.ACCEPTED);
//     }


//     @PostMapping(path = "/login")
//     public ResponseEntity<AppUser> login(@RequestParam("email") String email, @RequestParam("password") String password) {
//         if(email.equals("") || password.equals("")) return new ResponseEntity<>(HttpStatus.ACCEPTED);
//         AppUser user;
//         ResponseEntity<AppUser> responseUser;
//         boolean validateResult = appUserService.validateStudent(email, password);
//         System.out.println(" ********** Login Validation Result : " + validateResult);

//         // TODO: Enable "j_spring_security_check" for encrypted password security
//         user = appUserService.findUserByEmail(email);
//         if (validateResult == true && user != null) {
//             // String passEncrypted = new PasswordUtil().encryptPassword(password);
//             // System.out.println("***** Login User Info Email:"+user.getEmail()+"
//             // EncryptPassword: "+user.getPassword());
//             // System.out.println("PasswordEncrypt: "+passEncrypted);
//             // System.out.println("Debug: "+ user.getPassword().equals(passEncrypted));
//             responseUser = new ResponseEntity<>(user, HttpStatus.OK);
//             System.out.println("******* LOGIN SUCCESSFUL Welcome "+user.getEmail());
//         } else if(user == null){
//             System.err.println("This user is not existed. User = "+user );
//             responseUser = new ResponseEntity<>(HttpStatus.ACCEPTED);
//         }else {
//             responseUser = new ResponseEntity<>(HttpStatus.ACCEPTED);
//         }

//         return responseUser;
//     }
// }
