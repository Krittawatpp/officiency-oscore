package boot.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import boot.entity.user.AppUser;

@Repository
@Transactional
public class AppUserDao {
    @Autowired
    private EntityManager entityManager;

    public AppUser findUserByEmail(String email) {
        try {
            String sql = "select e from user e where e.email = :email";
            Query query = entityManager.createQuery(sql, AppUser.class);
            query.setParameter("email", email);
            return (AppUser) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public AppUser findUserByEmailorUsername(String input) {
        try {
            String sql = "select e from user e where e.email = :input or e.username = :input";
            Query query = entityManager.createQuery(sql, AppUser.class);
            query.setParameter("input", input);
            return (AppUser) query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
