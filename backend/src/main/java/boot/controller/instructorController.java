package boot.controller;

import java.util.Collection;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import boot.entity.user.AppUser;
import boot.entity.user.InstructorSkill;
import boot.entity.user.UserAllSkillScore;
import boot.entity.user.UserScoreByWorkshopAtt;
import boot.service.JsonService;
import boot.service.user.AppUserService;
import boot.service.user.InstructorService;
import boot.service.user.UserAllSkillScoreService;
import boot.service.user.UserScoreByWorkshopAttService;
import boot.service.workshop.*;
import boot.util.PasswordUtil;
import boot.entity.workshop.*;

@RestController
@CrossOrigin
public class instructorController {
    @Autowired
    private WorkshopService workshopService;
    @Autowired
    private AppUserService appUserService;
    @Autowired
    private UserAllSkillScoreService userAllSkillScoreService;
    @Autowired
    private WorkshopSkillForReviewService workshopSkillForReviewService;
    @Autowired
    private InstructorService instructorService;

    @RequestMapping(value = "/api/getAllInstructorSkills")
    Collection<InstructorSkill> getAllInstructorSkills() {

        return instructorService.getAllSkillsOfInstructor();
    }

    @RequestMapping(value = "/api/getUserByWorkshopId/{workshop_id}")
    AppUser getUserByWorkshopId(@PathVariable("workshop_id") Long workshopId) {
        Workshop workshop = workshopService.findWorkshopByWorkshopId(workshopId);
        AppUser user = appUserService.findUserByMyId(workshop.getCreateById());
        return user;
    }

    // Prach // CHECK!
    @RequestMapping(value = "/api/getUserAll", method = RequestMethod.GET)
    Collection<AppUser> getUserAll() {
        Collection<AppUser> allUser = appUserService.findAllUser();
        return allUser;
    }

    // แสดง profile ของ user นั้นๆ by 'P' // CHECK!
    @RequestMapping(value = "/api/getUserById/{user_id}")
    AppUser getUserById(@PathVariable("user_id") Long userId) {
        AppUser user = appUserService.findUserByMyId(userId);
        return user;
    }

    // Done!! // CHECK!
    // แสดง score เฉลี่ยของ user นั้นๆ by 'P'
    // ใช้แสดง score ใน profile
    @RequestMapping(value = "/api/getAllUserScoreById/{user_id}", method = RequestMethod.GET)
    List<UserAllSkillScore> getUserAllSkillScorre(@PathVariable("user_id") Long myId) {
        return userAllSkillScoreService.findUserAllSkillScoreById(myId);
    }

    // @RequestMapping(value = "/api/getInstructroByWorkshopId/{workshop_id}",
    // method = RequestMethod.GET)
    // AppUser getInstructroByWorkshopId(@PathVariable("workshop_id") Long
    // workshopId) {
    // return null;
    // }

    // TODO เพื่มหน้า show instructor เพื่อ review

    // TODO เพิ่มหน้า review instructor

    // TODO เพิ่มหน้า edit profile
    // edit profile by 'P'
    // ใช้กับ modal edit profile ในหน้า profile
    @RequestMapping(path = "/editprofile", method = RequestMethod.POST)
    public ResponseEntity<String> editprofile(@RequestParam("user_id") Long userId, @RequestParam("email") String email,
            @RequestParam("firstname") String firstname, @RequestParam("lastname") String lastname,
            @RequestParam("nickname") String nickname, @RequestParam("tel") String tel) {
        int user;
        try {
            user = appUserService.editProfile(userId, email, firstname, lastname, nickname, tel);
        } catch (Exception ex) {
            return new ResponseEntity<>("{\"status\" : \"" + ex.getMessage() + "\"}", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("{\"status\" : \"success\", \"updated_row\" : " + user + "}", HttpStatus.OK);
    }

}