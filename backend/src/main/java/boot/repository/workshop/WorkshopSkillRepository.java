package boot.repository.workshop;

import java.util.Collection;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import boot.entity.workshop.WorkshopSkill;

public interface WorkshopSkillRepository extends CrudRepository<WorkshopSkill, Long> {
    @Transactional
    @Modifying
    @Query(value = "INSERT INTO workshop_skill (create_by_id, workshop_id, skill_id) VALUES (?1, ?2, ?3);", nativeQuery = true)
    public int addWorkshopSkill(int userId, int workshopId, int skill_id);

}