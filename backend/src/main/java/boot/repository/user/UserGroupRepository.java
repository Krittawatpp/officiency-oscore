package boot.repository.user;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.UserGroup;

public interface UserGroupRepository extends CrudRepository<UserGroup, Long> {

    String sqlforShow = "SELECT	user_group.id, " + "user_group.user_id, " + "`user`.email, " + "`user`.firstname, "
            + "`user`.lastname, " + "user_group.workshop_id, " + "user_group.group_number "
            + "FROM user_group LEFT JOIN `user` " + "on user_group.user_id = `user`.user_id "
            + "WHERE user_group.workshop_id = ?1 " + "and user_group.group_number = ?2";

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO user_group (user_id, workshop_id, group_number) VALUES (?1, ?2, ?3)", nativeQuery = true)
    public int addUserGroupByWorkshopAndGroupId(Long userId, Long workshopId, int groupNumber);

    @Query(value = sqlforShow, nativeQuery = true)
    public Collection<UserGroup> showUserInSameGroupByWorkshopIdandGroupNumber(Long workshopId, Long groupNumber);
}