package boot.repository.user;

import java.util.Collection;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.UserFeedback;

public interface UserFeedbackRepository extends CrudRepository<UserFeedback, Long> {

    String sqlForShowFeedback = "SELECT * FROM " + "user_feedback WHERE " + "workshop_attend_id = ?2 "
            + "AND user_id = ?1";

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO user_feedback  (user_id,workshop_attend_id, group_number, feedback, suggestion) values (?1 , ?2, ?3, ?4, ?5);", nativeQuery = true)
    public int insertUserFeedback(Long userId, Long workshopId, Long groupNumber, String feedback, String suggestion);

    @Query(value = sqlForShowFeedback, nativeQuery = true)
    public Collection<UserFeedback> showUserFeedbackForCurrentWorkshop(Long userId, Long workshopId);

}