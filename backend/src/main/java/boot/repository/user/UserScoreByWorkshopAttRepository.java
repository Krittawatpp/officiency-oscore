package boot.repository.user;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import boot.entity.user.UserScoreByWorkshopAtt;

public interface UserScoreByWorkshopAttRepository extends CrudRepository<UserScoreByWorkshopAtt, Long> {

    String sql = "SELECT MIN(user_att_workshop.id) as id, " + "user_att_workshop.user_id, " + "skill.skill_name, "
            + "AVG(user_att_workshop.skill_score) as avgscore " + "from user_att_workshop left join skill "
            + "on user_att_workshop.skill_id = skill.id " + "GROUP BY user_att_workshop.user_id, " + "skill.id, "
            + "user_att_workshop.workshop_att_id " + "HAVING user_att_workshop.user_id = ?1 "
            + "and user_att_workshop.workshop_att_id = ?2 " + "ORDER BY skill.id";

    // @Query(value = "SELECT user_att_workshop.id, user_att_workshop.user_id,
    // user_att_workshop.workshop_att_id, skill.skill_name,
    // user_att_workshop.skill_score FROM user_att_workshop LEFT JOIN skill on
    // user_att_workshop.skill_id = skill.id WHERE user_att_workshop.user_id = ?5
    // AND user_att_workshop.workshop_att_id = ?1;", nativeQuery = true)
    // public Collection<UserScoreByWorkshopAtt>
    // findUserScoreByUserIdAndWorkshopId(Long userId, Long workshopId);

    @Query(value = sql, nativeQuery = true)
    public Collection<UserScoreByWorkshopAtt> findUserScoreByUserIdAndWorkshopId(Long userId, Long workshopId);

    // public Collection<UserScoreByWorkshopAtt>
    // findUserScoreByUserIdAndWorkshopId();

}