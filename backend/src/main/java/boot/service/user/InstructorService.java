package boot.service.user;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.user.InstructorSkill;
import boot.repository.InstructorSkillsRepository;
import boot.repository.user.InstructorAttWorkshopRepository;
import boot.repository.user.UserFeedbackRepository;

@Service
public class InstructorService {
    @Autowired
    private InstructorSkillsRepository  instructorSkillsRepository;
    @Autowired
    private InstructorAttWorkshopRepository instructorAttWorkshopRepository;
    @Autowired
    private UserFeedbackRepository userFeedbackRepository;

    //Used after student finish their reviewed for instructor
    public int insertFeedbackForInstructor(Long instructorId, Long workshopId, Long groupNumber, String feedback, String suggestion) {
        return userFeedbackRepository.insertUserFeedback(instructorId, workshopId, groupNumber, feedback, suggestion);
    }

    //Used after student finish their reviewed for instructor
    public int insertInstructorScore(Long instructorId, Long workshopId, Long skillId, Long skillScore) {
        return instructorAttWorkshopRepository.insertInstructorScore(instructorId, workshopId, skillId, skillScore);
    }

    //Used on Review Instructor page
    public Collection<InstructorSkill> getAllSkillsOfInstructor() {
        return instructorSkillsRepository.getAllSkills();
    }
}