package boot.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.repository.user.UserInGroupRepository;

@Service
public class UserInGroupService {
    @Autowired
    UserInGroupRepository userInGroupRepository;

    public int leaveGroup(Long userId, Long workshopId) {
        return userInGroupRepository.leaveGroup(userId, workshopId);
    }
}