package boot.service.workshop;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.workshop.Workshop;
import boot.repository.workshop.WorkshopRepository;

@Service
public class WorkshopService {
    @Autowired
    private WorkshopRepository workshopRepository;

    public Iterable<Workshop> findAllWorkShops() {
        Iterable<Workshop> allWorkShops = workshopRepository.findAll();
        return allWorkShops;
    }

    public Workshop findWorkshopByWorkshopId(Long workshopId) {
        return workshopRepository.findWorkshopByWorkshopId(workshopId);
    }

    public Iterable<Workshop> findWorkShopByCreatedId(Long id) {
        Iterable<Workshop> workshops = workshopRepository.findWorkShopByCreatedId(id);
        return workshops;
    }

    // public Workshop addWorkshop(Long createbyid, String title, String
    // description, Date start, Date end) {
    // Workshop addWorkshop = new Workshop(createbyid, title, description, start,
    // end);
    // return workshopRepository.save(addWorkshop);
    // }

    public int addWorkshop(Long userId, String title, String start) {
        return workshopRepository.addNewWorkshop(userId, title, start);
    }

    public Workshop findWorkshopByCreateIdandTitle(Long userId, String title) {
        return workshopRepository.findWorkshopByCreateIdandTitle(userId, title);
    }

    public int editWorkshop(Long id, String title, String desc, String start, String end) {
        return workshopRepository.editWorkshop(id, title, desc, start, end);
    }

    public int deleteWorkshop(Long id) {
        return workshopRepository.deleteWorkshop(id);
    }
}