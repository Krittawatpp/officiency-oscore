package boot.service.workshop;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.workshop.WorkshopSkillForReview;
import boot.repository.workshop.WorkshopSkillForReviewRepository;

@Service
public class WorkshopSkillForReviewService {

    @Autowired
    private WorkshopSkillForReviewRepository workshopSkillForReviewRepository;

    public Collection<WorkshopSkillForReview> findWorkshopSkillByWorkshopId(Long workshopId) {
        return workshopSkillForReviewRepository.findWorkshopSkillByWorkshopId(workshopId);
    }
}
