package boot.service.workshop;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.workshop.WorkshopSkill;
import boot.repository.workshop.WorkshopSkillRepository;

@Service
public class WorkshopSkillService {
    @Autowired
    private WorkshopSkillRepository workshopSkillRepository;

    public int addWorkshopSkill(int userId, int workshopId, int skillId) {
        return workshopSkillRepository.addWorkshopSkill(userId, workshopId, skillId);
    }

}
