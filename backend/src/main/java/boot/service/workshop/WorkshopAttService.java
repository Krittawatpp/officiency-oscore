package boot.service.workshop;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.workshop.WorkshopAtt;
import boot.repository.workshop.WorkshopAttRepository;

@Service
public class WorkshopAttService {

    @Autowired
    private WorkshopAttRepository workshopAttRepository;

    public Collection<WorkshopAtt> findAllWorkshopAtt() {
        Collection<WorkshopAtt> allWorkshopAtt = workshopAttRepository.findAllWorkshopAtt();
        return allWorkshopAtt;
    }

    public Collection<WorkshopAtt> findWorkshopAttById(Long userId) {
        Collection<WorkshopAtt> workshopAtt = workshopAttRepository.findAllWorkshopAttById(userId);
        return workshopAtt;
    }
}
