package boot.service.workshop;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import boot.entity.workshop.WorkshopAttToProfile;
import boot.repository.workshop.WorkshopAttToProfileRepository;

@Service
public class WorkshopAttToProfileService {
    @Autowired
    private WorkshopAttToProfileRepository workshopAttToProfileRepository;

    public List<WorkshopAttToProfile> findWorkshopAttToProfile(Long userId) {
        return workshopAttToProfileRepository.findWorkshopAttToProfileById(userId);
    }
}