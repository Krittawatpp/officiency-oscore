package boot.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UserAllSkillScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "user_id", nullable = false)
    private Long userId;
    @Column(name = "skill_name", nullable = false)
    private String skillName;
    @Column(name = "avgscore", nullable = false)
    private Long avgScore;

    private UserAllSkillScore() {

    }

    private UserAllSkillScore(Long id, Long userId, String skillName, Long avgScore) {
        this.id = id;
        this.userId = userId;
        this.skillName = skillName;
        this.avgScore = avgScore;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return this.userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getSkillName() {
        return this.skillName;
    }

    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    public Long getAvgScore() {
        return this.avgScore;
    }

    public void setAvgScore(Long avgScore) {
        this.avgScore = avgScore;
    }

}